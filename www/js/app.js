// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic','starter.controllers', 'ionic-material','countTo','services','ionic-datepicker','ui.utils.masks']);

app.run(function ($rootScope, $ionicPlatform, $ionicHistory,$ionicLoading) {


  $ionicPlatform.registerBackButtonAction(function(e){
    if ($rootScope.backButtonPressedOnceToExit) {
      navigator.app.exitApp();
  }
  else {
    $rootScope.backButtonPressedOnceToExit = true;
    $ionicLoading.show({ template: 'Çıkmak için tekrar basın', noBackdrop: true, duration: 1000 });
    setTimeout(function(){
        $rootScope.backButtonPressedOnceToExit = false;
    },2000);
}

},101);

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

  }
  if (window.StatusBar) {
    if (ionic.Platform.isAndroid()) {
      StatusBar.backgroundColorByHexString("#F44336");
    } else {
      StatusBar.styleLightContent();
    }
  }
     ////

 
    //FCMPlugin.getToken( successCallback(token), errorCallback(err) );
    //Keep in mind the function will return null if the token has not been established yet.
    if (typeof FCMPlugin != 'undefined') {
     FCMPlugin.getToken(
        function (token) {
            $rootScope.fcmToken = token;
            console.log('fcm token: ', $rootScope.fcmToken);
            
        },
        function (err) {
         console.log('error retrieving token: ' + token);
         console.log('error retrieving token: ' + err);
     }
     );

     FCMPlugin.onNotification(
        function(data){
            if(data.wasTapped){
        //Notification was received on device tray and tapped by the user.
        console.log("Tapped: " +  JSON.stringify(data) );
        
    }else{
        //Notification was received in foreground. Maybe the user needs to be notified.
        console.log("Not tapped: " + JSON.stringify(data) );

    }
},
function(msg){
    console.log('onNotification callback successfully registered: ' + msg);
},
function(err){
    console.log('Error registering onNotification callback: ' + err);
}
);
 }
    ////
});

})

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    .state('app.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl',
                resolve:{
                   "check":function($location){  
                     if(localStorage.getItem('userDetails')){ $location.path('/app/homepage');   }
                     else
                        {  $location.path('/app/login');   }

                }
            }
        }
    }
})

    .state('app.homepage', {
        url: '/homepage',
        views: {
            'menuContent': {
                templateUrl: 'templates/homepage.html',
                controller: 'HomepageCtrl'
            }
        }
    })

    .state('app.account', {
        url: '/account',
        views: {
            'menuContent': {
                templateUrl: 'templates/account.html',
                controller: 'AccountCtrl'
            }
        }
    })
    .state('app.banaozel', {
        url: '/banaozel',
        views: {
            'menuContent': {
                templateUrl: 'templates/banaozel.html',
                controller: 'BanaozelCtrl'
            }
        }
    })

    .state('app.kabulettiklerim', {
        url: '/kabulettiklerim',
        views: {
            'menuContent': {
                templateUrl: 'templates/kabulettiklerim.html',
                controller: 'KabulettiklerimCtrl'
            }
        }
    })

    .state('app.tumbakicilar', {
        url: '/tumbakicilar',
        views: {
            'menuContent': {
                templateUrl: 'templates/tumbakicilar.html',
                controller: 'TumbakicilarCtrl'
            }
        }
    })

    .state('app.bakicidetay', {
        url: '/bakicidetay/:bakiciID',
        views: {
            'menuContent': {
                templateUrl: 'templates/bakicidetay.html',
                controller: 'BakicidetayCtrl'
            }
        }
    })    

    .state('app.teklifgonderdiklerim', {
        url: '/teklifgonderdiklerim',
        views: {
            'menuContent': {
                templateUrl: 'templates/teklifgonderdiklerim.html',
                controller: 'TeklifgonderdiklerimCtrl'
            }
        }
    })

    .state('app.kabuledenler', {
        url: '/kabuledenler',
        views: {
            'menuContent': {
                templateUrl: 'templates/kabuledenler.html',
                controller: 'KabuledenlerCtrl'
            }
        }
    })

    .state('app.teklifolustur', {
        url: '/teklifolustur',
        views: {
            'menuContent': {
                templateUrl: 'templates/teklifolustur.html',
                controller: 'TeklifolusturCtrl'
            }
        }
    })

    .state('app.onaylanmistekliflerim', {
        url: '/onaylanmistekliflerim',
        views: {
            'menuContent': {
                templateUrl: 'templates/onaylanmistekliflerim.html',
                controller: 'OnaylanmistekliflerimCtrl'
            }
        }
    })

    .state('app.onaybekleyentekliflerim', {
        url: '/onaybekleyentekliflerim',
        views: {
            'menuContent': {
                templateUrl: 'templates/onaybekleyentekliflerim.html',
                controller: 'OnaybekleyentekliflerimCtrl'
            }
        }
    })

    .state('app.teklifdetay', {
        url: '/teklifdetay/:serviceID/:category',
        views: {
            'menuContent': {
                templateUrl: 'templates/teklifdetay.html',
                controller: 'TeklifdetayCtrl'
            }
        }
    })

/*    .state('app.message', {
        url: '/message',
        views: {
            'menuContent': {
                templateUrl: 'templates/message.html',
                controller: 'MessageCtrl'
            }
        }
    })
*/
    .state('app.chats', {
      url: '/chats/:senderID/:receiverID',
      views: {
        'menuContent': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'MessageCtrl'
      }
  }
})
/*    .state('app.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'menuContent': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
  })*/
  
  .state('app.bakiciekle', {
    url: '/bakiciekle',
    views: {
        'menuContent': {
            templateUrl: 'templates/firmabakiciekle.html',
            controller: 'FirmaBakiciEkleCtrl'
        }
    }
})



  ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
});
