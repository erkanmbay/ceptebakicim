app.controller('FirmaBakiciEkleCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout, ionicMaterialInk, ionicMaterialMotion, ionicDatePicker,Service) {

	ionicMaterialMotion.blinds();
	/*   $rootScope.banaOzelTekliflerFon();*/
    Service.GetCitys().then(function(data){
        $rootScope.citys=data;
    })

    $scope.getDistrictFon = function(city){
    Service.GetDistricts(city).then(function(data){
        $rootScope.districts=data;
    })
    } 

	$rootScope.nations = [
	{ "nation": "Azerbaycan"},
	{ "nation": "Gürcistan"},
	{ "nation": "Özbekistan"},
	{ "nation": "Rusya" },
	{ "nation": "Türkmenistan"}	
	];
	$rootScope.languages =[
	{ "language": "Türkçe"},
	{ "language": "Rusça"},
	{ "language": "İngilizce"},
	{ "language": "Türkçe / İngilizce" },
	{ "language": "Türkçe / Rusça"},
	{ "language": "Türkçe / İngilizce /Rusça"}	
	];
	$rootScope.abilities=[
	{ "ability": "Yapar"},
	{ "ability": "Yapamaz"}
	];
	$rootScope.permissions=[
	{ "permission": "Var"},
	{ "permission": "Yok"}
	];
	$rootScope.works=[
	{ "work": "Çalışır"},
	{ "work": "Çalışmaz"}
	];	
	$rootScope.smoke=[
	{ "smoke": "Kullanıyor"},
	{ "smoke": "Kullanmıyor"},
	{ "smoke": "Mesai Saatleri Dışında"}
	];
	$rootScope.workTypes=[
	{ "workType": "Gündüz"},
	{ "workType": "Yatılı"}
	];		
	$scope.regexDate = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
	// Return today's date and time
	var currentTime = new Date();
	// returns the year (four digits)
	var year = currentTime.getFullYear()
	$rootScope.years = []; // or the shortcut: = []
	for(var i =1930; i<=year-18;i++){
		$rootScope.years.push ( {"year":i} );
	}
	$rootScope.salary = []; // or the shortcut: = []
	for(var i =100; i<=1000;i+=100){
		$rootScope.salary.push ( {"salary":"$ " + i } );
	}
	$scope.dataBakiciEkle = {};
	$scope.firmaBakiciEkleFon = function(){
		var link = $rootScope.API+'firmaBakiciEkle.php';
		$http.post(link, {
			name:$scope.dataBakiciEkle.name,
			email:$scope.dataBakiciEkle.email,
			pass:$scope.dataBakiciEkle.pass,
			phone:$scope.dataBakiciEkle.phone,
			yas: new Date($scope.dataBakiciEkle.yas).toJSON().slice(0,10),
			uyruk:$scope.dataBakiciEkle.uyruk,
			pasaport:$scope.dataBakiciEkle.pasaport,
			image:"default-profile-picture.png",
			ilAdi:$scope.dataBakiciEkle.ilAdi,
			ilceAdi:$scope.dataBakiciEkle.ilceAdi,
			adres:$scope.dataBakiciEkle.adres,
			cocukBakim:$scope.dataBakiciEkle.cocukBakim,
			hastaBakim:$scope.dataBakiciEkle.hastaBakim,
			yasliBakim:$scope.dataBakiciEkle.yasliBakim,
			temizlik:$scope.dataBakiciEkle.temizlik,
			yemek:$scope.dataBakiciEkle.yemek,
			utu:$scope.dataBakiciEkle.utu,
			deneyim:$scope.dataBakiciEkle.deneyim,
			sigara:$scope.dataBakiciEkle.sigara,
			maas:$scope.dataBakiciEkle.maas,
			calismaS:$scope.dataBakiciEkle.calismaS,
			dil:$scope.dataBakiciEkle.dil,
			sehirD:$scope.dataBakiciEkle.sehirD,
			oturum:$scope.dataBakiciEkle.oturum,
			eFirma:$rootScope.local_userDetails.id
		})
		.then(function(res) {
			$scope.firmaBakiciEkleResponse = res.data.status;
			if ($scope.firmaBakiciEkleResponse == true) {
				$rootScope.loadData('Kayıt başarılı!', 'ion-checkmark-round');
				console.log("dataBakiciEkle: ", $scope.dataBakiciEkle);
				$scope.dataBakiciEkle = {};
			} else {
				$rootScope.loadData('Kayıt başarısız!', 'ion-close-round');
				console.log("dataBakiciEkle: ", $scope.dataBakiciEkle);
			}
		})
	};

	$scope.takePic = function() {
		var options =   {
			quality: 50,
			destinationType: Camera.DestinationType.DATA_URL,
        sourceType: 0,      // 0:Photo Library, 1=Camera, 2=Saved Photo Album
        encodingType: 0     // 0=JPG 1=PNG
    }
    // Take picture using device camera and retrieve image as base64-encoded string
    navigator.camera.getPicture(onSuccess,onFail,options);
}
var onSuccess = function(imageData) {
	console.log("On Success! ");
	$scope.picData = "data:image/jpeg;base64," +imageData;
	$scope.$apply();
};



})

