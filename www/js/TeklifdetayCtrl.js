app.controller('TeklifdetayCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout, ionicMaterialInk, ionicMaterialMotion,Service) {

	ionicMaterialMotion.blinds();

	var serviceID = $stateParams.serviceID;
	var category = $stateParams.category;
	$scope.category = $stateParams.category;

	if(category == "banaozelteklifler"){
		$rootScope.teklif = Service.GetBanaOzelTeklif(serviceID);
	}
	else if(category == "kabulettigimteklifler"){
		$rootScope.teklif = Service.GetKabulEttigimTeklif(serviceID);
	}
	else if(category == "teklifgonderdiklerim"){
		$rootScope.teklif = Service.GetTeklifGonderdigim(serviceID);
	}
	else if(category == "kabuledenler"){
		$rootScope.teklif = Service.GetKabulEden(serviceID);
	}
	else if(category == "onaylanmisteklifler"){
		$rootScope.teklif = Service.GetOnaylanmisTeklif(serviceID);
	}
	else if(category == "onaybekleyenteklifler"){
		$rootScope.teklif = Service.GetOnaylanmisTeklif(serviceID);
	}
	
$rootScope.bakiciteklifIslem = function (interviewID,sorgu,userID){
	console.log("bakiciteklifIslem: ",interviewID,sorgu,userID)
	$http.get($rootScope.API+'bakiciteklifIslem.php?interviewID='+interviewID+'&sorgu='+sorgu+'&userID='+userID)
	.success(function(data){
		$rootScope.bakiciTeklifOnayla=data;
		if(sorgu==1){
			$rootScope.loadData('Teklif Onaylandı','ion-checkmark-round');
		}
		$scope.doRefresh('kabulettigimteklifler');
		$rootScope.bakiciCalismaDurumuFon();
		if(sorgu==0){
			$rootScope.loadData('Teklif Reddedildi','ion-close-round');
		}
		$state.go('app.banaozel', {}, {location: "replace", reload: true});

		$scope.doRefresh('banaozelteklifler');
	})
}


})
