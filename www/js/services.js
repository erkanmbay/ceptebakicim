var services = angular.module('services', [])

/*.factory('sharedFilterService', [function(){
	var API="http://192.168.1.10/ceptebakicim/json/";
	var obj = {};
  obj.str = API+"userList2.php?userType=2&till=";
  obj.sort = "";
  obj.search = "";
  obj.category = "";
  obj.till=0;



  obj.getUrl=function(eFirma,localUserType){

    obj.till=obj.till + 15;
		obj.str= API+"userList2.php?userType=2&till="+obj.till+"&eFirma="+eFirma+"&localUserType="+localUserType; // pass the value to url
		
		if(obj.sort!="" && obj.category!=""){
			obj.str= obj.str+"&category="+obj.category+"&sort="+obj.sort;
		}
		else if(obj.category!="" ){
			obj.str= obj.str+"&category="+obj.category;
		}
		else if(obj.sort!=""){  
			obj.str= obj.str+"&sort="+obj.sort;
		}
		console.log("URL",obj.str);
		return obj.str;
	};
	return obj;
}])*/

.factory('Service',['$http', function($http,$scope){
  var API="http://www.erkanmbay.com/ceptebakicim/json/";
  var till = 0;
    var bakicilar = {}; //Private Variable
    return {
      GetBakicilar: function(eFirma,localUserType){ 
        till = till + 15;
        return $http.get(API + "userList2.php?userType=2&till="+till+"&eFirma="+eFirma+"&localUserType="+localUserType).then(function(donus){
          bakicilar = donus.data;
          return donus.data;
        });
      },
      GetBakici: function(personId){
        for(i=0;i<bakicilar.length;i++){
          if(bakicilar[i].id == personId){
            return bakicilar[i];
          }
        }
      },
      GetHomePage: function(userType,id){ 
        till = till + 15;
        return $http.get(API + 'home.php?userType='+userType+'&userID='+id).then(function(response){
          homepage = response.data;
          return response.data;
        });
      },
      GetKabulEttigimTeklifler: function(id){
        return $http.get(API+'bakiciTeklifler.php?userID='+id+'&teklif='+1).then(function(response){
          kabulettigimteklifler = response.data;
          return response.data
        })
      },
      GetKabulEttigimTeklif: function(teklifID){
        for(i=0;i<kabulettigimteklifler.length;i++){
          if(kabulettigimteklifler[i].serviceID == teklifID){
            return kabulettigimteklifler[i];
          }
        }
      },
      GetBanaOzelTeklifler: function(id){
        return $http.get(API+'bakiciTeklifler.php?userID='+id+'&teklif='+0).then(function(response){
          banaozelteklifler = response.data;
          return response.data
        })
      },
      GetBanaOzelTeklif: function(teklifID){
        for(i=0;i<banaozelteklifler.length;i++){
          if(banaozelteklifler[i].serviceID == teklifID){
            return banaozelteklifler[i];
          }
        }
      },
      GetTeklifGonderdiklerim: function(id){
        return $http.get(API+'AileBakiciTeklif.php?userID='+id+'&teklif='+1).then(function(response){
          teklifgonderdiklerim = response.data;
          return response.data
        })
      },
      GetTeklifGonderdigim: function(teklifID){
        for(i=0;i<teklifgonderdiklerim.length;i++){
          if(teklifgonderdiklerim[i].serviceID == teklifID){
            return teklifgonderdiklerim[i];
          }
        }
      },
      GetKabulEdenler: function(id){
        return $http.get(API+'AileBakiciTeklif.php?userID='+id+'&teklif='+2).then(function(response){
          kabuledenler = response.data;
          return response.data
        })
      },
      GetKabulEden: function(teklifID){
        for(i=0;i<kabuledenler.length;i++){
          if(kabuledenler[i].serviceID == teklifID){
            return kabuledenler[i];
          }
        }
      },
      GetOnaylanmisTekliflerim: function(id){
        return $http.get(API+'aileTeklifler.php?userID='+id+'&teklif='+2).then(function(response){
          onaylanmistekliflerim = response.data;
          return response.data
        })
      },
      GetOnaylanmisTeklif: function(teklifID){
        for(i=0;i<onaylanmistekliflerim.length;i++){
          if(onaylanmistekliflerim[i].serviceID == teklifID){
            return onaylanmistekliflerim[i];
          }
        }
      },      
      GetOnayBekleyenTeklifler: function(id){
        return $http.get(API+'aileTeklifler.php?userID='+id+'&teklif='+1).then(function(response){
          onaylanmistekliflerim = response.data;
          return response.data
        })
      },
      GetOnayBekleyenTeklif: function(teklifID){
        for(i=0;i<onaylanmistekliflerim.length;i++){
          if(onaylanmistekliflerim[i].serviceID == teklifID){
            return onaylanmistekliflerim[i]
          }
        }
      },
      GetCitys: function(){
        return $http.get('json/citys.json').then(function(response){
          citys = response.data;
          return response.data
        })
      },
      GetDistricts: function(city){
       var districts=[];
        return $http.get('json/citydistricts.json').then(function(response){
          citydistricts = response.data;
        for(i=0;i<citydistricts.length;i++){
          if(citydistricts[i].city == city){
            districts.push({"district_name": citydistricts[i].district_name});
          }
        }
        return districts
        })
      }


    }
  }])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array
  var chats = [];

  return {
    all: function(familyID,caryID) {
      console.log("familyID,caryID", familyID,caryID);
      chats = [];

      var roomName = "1-2";
      var bakicimRef = firebase.database().ref("Chats/").child(roomName);

      bakicimRef.orderByChild("gonderen").on("child_added", function(data) {

        chats.push(data.val());


      });

      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});