//Bakıcılar
app.controller('TumbakicilarCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout, ionicMaterialInk, ionicMaterialMotion, Service) /*sharedFilterService*/{
    ionicMaterialMotion.blinds();
   // $rootScope.tumBakicilarFon();  

    $scope.noMoreItemsAvailable = false; // lazy load list        
    //loads the menu----onload event
  /*  $scope.$on('$stateChangeSuccess', function() {
        $scope.loadMore($rootScope.local_userDetails.id,$rootScope.local_userDetails.userType);  //Added Infine Scroll
    });*/
     
    // Loadmore() called inorder to load the list 
/*    $rootScope.loadMore = function(eFirma,localUserType) {  
            str=sharedFilterService.getUrl(eFirma,localUserType);

            $http.get(str).success(function (response){
                console.log("loadMore response:", response);
                $scope.tumbakicilar = response;
               // $rootScope.hasmore!=0;   //"has_more": 0 or number of items left
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }); 
            
            //more data can be loaded or not
            if ($scope.hasmore == 0 ) {
              $scope.noMoreItemsAvailable = true;
            }
    };*/

    //

    $rootScope.loadMore = function(eFirma,localUserType){
        console.log("loadMore ctrl");

        Service.GetBakicilar(eFirma,localUserType).then(function(bakicilar){
            $scope.tumbakicilar = bakicilar;
            $scope.$broadcast('scroll.infiniteScrollComplete');
            console.log("bakicilar", $scope.tumbakicilar);
        });

         //more data can be loaded or not
         if ($scope.hasmore == 0 ) {
          $scope.noMoreItemsAvailable = true;
      }
    };
  console.log("Tüm bakıcılar Controller");

})

app.controller('BakicidetayCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout, ionicMaterialInk, ionicMaterialMotion,Service){
    //bakici detayı
    var bakiciID = $stateParams.bakiciID;
    $scope.bakici = Service.GetBakici(bakiciID);

    $rootScope.onaylanmisTekliflerimFon();

/*TeklifVer*/
    // Form data for the teklifver modal
    $scope.dataTeklifVer = {};

    // Create the teklif modal that we will use later
    $ionicModal.fromTemplateUrl('templates/teklifvermodal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.teklifvermodal = modal;
    });

    // Open the login modal
    $scope.openteklifvermodal = function() {
        $scope.teklifvermodal.show();
        console.log("teklivermodal");
        // $timeout(function () {
        //             $scope.teklifvermodal.hide();
        //         }, 2000);
    };
    // Triggered in the login modal to close it
    $scope.closeteklifvermodal = function() {
        $scope.teklifvermodal.hide();
        $scope.dataTeklifVer = {};
    };
    // Perform the submit action when the user submits the teklif form
    $scope.teklifVer = function() {
      console.log('teklifVeriliyor');

      $http.post($rootScope.API+'AileTeklifGonder.php?serviceID='+$scope.dataTeklifVer.teklif
        +'&familyID='+$rootScope.local_userDetails.id
        +'&caryID='+$scope.bakici.id)
    .success(function(data) { 
      $scope.response=data; 
      console.log("teklifVer response: ", $scope.response);
        if($scope.response == true){
          $rootScope.loadData('Teklif Gönderildi!','ion-checkmark-round');
          $rootScope.closePageFon('homepage');
        }
        else{
            $rootScope.loadData('Teklif Gönderilemedi!','ion-close-round');
        $rootScope.closePageFon('tumbakicilar');

        }
        $timeout(function(){ $scope.closeteklifvermodal();$scope.dataTeklifVer = {}; },1500);      
    })
    };
    // Cleanup the modal when we're done with it
    // $scope.$on('$destroy', function() {
    //     $scope.teklifvermodal.remove();
    // });
/*TeklifVer*/

})

app.controller('TeklifgonderdiklerimCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout, ionicMaterialInk, ionicMaterialMotion) {

    ionicMaterialMotion.blinds();

   $rootScope.teklifGonderdiklerimFon();  

})

app.controller('KabuledenlerCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout, ionicMaterialInk, ionicMaterialMotion) {

    ionicMaterialMotion.blinds();

   $rootScope.kabulEdenlerFon();


    $scope.istenCikartmaSebebi = {};
        // Confirm
    $scope.istenCikartPopup = function() {
      var promptPopup = $ionicPopup.prompt({
         title: 'İşten Çıkarma Nedeni',
         template:  '<ion-radio ng-model="data.choice" ng-value="1">Uygunsuz Davranış</ion-radio>'+
                    '<ion-radio ng-model="data.choice" ng-value="2">Cinsel Taciz</ion-radio>'+
                    '<ion-radio ng-model="data.choice" ng-value="3">Hırsızlık</ion-radio>'+
                    '<ion-radio ng-model="data.choice" ng-value="4">İhtiyaç Dışı</ion-radio>',
         scope: $scope,
         buttons: [{text: 'İptal',
                    type: 'button-assertive'},
                    {
                    text: '<b>Çıkart</b>',
                    type: 'button-balanced',
                    onTap: function(e) {
                        console.log($scope.istenCikartmaSebebi.choice); // 0
                        return $scope.istenCikartmaSebebi.choice;
                    }
                }]
      });
        
/*      promptPopup.then(function(data) {
         console.log($scope.data.choice);
      });*/
    };

})

//Tekliflerim
app.controller('TeklifolusturCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout, ionicMaterialInk, ionicMaterialMotion) {

    ionicMaterialMotion.blinds();

   console.log("teklif olustur controller");

/*TeklifOlustur*/
    // Form data for the teklif modal
    $scope.dataTeklifOlustur = {};
    // Create the teklif modal that we will use later
    $ionicModal.fromTemplateUrl('templates/teklifolusturmodal.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.teklifolusturmodal = modal;
    });
    // Open the login modal
    $scope.openteklifolusturmodal = function(teklifTipi) {
        $scope.teklifolusturmodal.show();
        // $timeout(function () {
        //             $scope.teklifolusturmodal.hide();
        //         }, 2000);
        $scope.teklifTipi = teklifTipi;
        console.log($scope.teklifTipi);
    };
    // Triggered in the login modal to close it
    $scope.closeteklifolusturmodal = function() {
        $scope.teklifolusturmodal.hide();
        $scope.dataTeklifOlustur = {};
    };
    // Perform the submit action when the user submits the teklif form
    $scope.submit = function(teklifTipi) {
        var link = $rootScope.API+'aileTeklifOlustur.php';
        console.log("teklif olustur", $scope.dataTeklifOlustur);

        switch (teklifTipi) {
            case 'Bebek / Çocuk Bakımı':
                $scope.teklif = 1;
                break;
            case 'Yaşlı Bakımı':
                $scope.teklif = 2;
                break;
            case 'Hasta Bakımı':
                $scope.teklif = 3;
                break;
            case 'Ev Temizliği':
                $scope.teklif = 4;
                break;
            default:
        }
        $http.post(link, {
                cocukYasAralik: $scope.dataTeklifOlustur.cocukYasAralik,
                cocukOkulDurumu: $scope.dataTeklifOlustur.cocukOkulDurumu,
                anneCalisiyorMu: $scope.dataTeklifOlustur.anneCalisiyorMu,
                cocukHastaMi: $scope.dataTeklifOlustur.cocukHastaMi,
                cocukHastalikDetay: $scope.dataTeklifOlustur.cocukHastalikDetay,
                yasliDurumu: $scope.dataTeklifOlustur.yasliDurumu,
                yapilmasiGerekenler: $scope.dataTeklifOlustur.yapilmasiGerekenler,
                hastalik: $scope.dataTeklifOlustur.hastalik,
                evturu: $scope.dataTeklifOlustur.evturu,
                kat: $scope.dataTeklifOlustur.kat,
                oda: $scope.dataTeklifOlustur.oda,
                duvar: $scope.dataTeklifOlustur.duvar,
                temizlik: $scope.dataTeklifOlustur.temizlik,
                yemek: $scope.dataTeklifOlustur.yemek,
                teklif: $scope.teklif,
                userID: $rootScope.local_userDetails.id
            })
            .then(function(res) {
                $scope.aileBakiciTeklifResponse = res.data.status;
                if ($scope.aileBakiciTeklifResponse == true) {
                    $rootScope.loadData('Kayıt başarılı!', 'ion-checkmark-round');
                    console.log("dataTeklifOlustur: ", $scope.dataTeklifOlustur);
                } else {
                    $rootScope.loadData('Kayıt başarısız!', 'ion-close-round');
                    console.log("dataTeklifOlustur: ", $scope.dataTeklifOlustur);
                }

            })
        $timeout(function() {
            $scope.closeteklifolusturmodal();
        }, 1500);
    };
    // Cleanup the modal when we're done with it
    $scope.$on('$destroy', function() {
        $scope.teklifolusturmodal.remove();
    });
/*TeklifOlustur*/
})

app.controller('OnaylanmistekliflerimCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout, ionicMaterialInk, ionicMaterialMotion) {

    ionicMaterialMotion.blinds();

   $rootScope.onaylanmisTekliflerimFon();

})

app.controller('OnaybekleyentekliflerimCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout, ionicMaterialInk, ionicMaterialMotion) {

    ionicMaterialMotion.blinds();

   $rootScope.onayBekleyenTekliflerimFon();

})
