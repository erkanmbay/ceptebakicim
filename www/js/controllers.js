angular.module('starter.controllers', [])


//material controllers
app.controller('AppCtrl', function ($scope, $ionicModal, $ionicPopover, $timeout,$rootScope,$ionicLoading,$state,$ionicHistory,$ionicPlatform,$ionicPopup,$http, ionicDatePicker,Service)/*sharedFilterService*/ {



    $rootScope.API="http://www.erkanmbay.com/ceptebakicim/json/";

    $http.get('json/citys.json').success(function(data){
        $rootScope.citys=data;
    });

    $http.get('json/citydistricts.json').success(function(data){
        $rootScope.districts=data;
    });

      //close modals
      $rootScope.closePageFon= function(page){
        $state.go('app.'+page, {}, {location: "replace", reload: true});
        $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: true
        }); 
    }
//close modals
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyBlDyS8CUDPQWf5OTv4JHT1yl36BbExmWs",
        authDomain: "ceptebakicim-ac60a.firebaseapp.com",
        databaseURL: "https://ceptebakicim-ac60a.firebaseio.com",
        projectId: "ceptebakicim-ac60a",
        storageBucket: "ceptebakicim-ac60a.appspot.com",
        messagingSenderId: "552406288112"
    };
    firebase.initializeApp(config);
    console.log(firebase.app());
      // Initialize Firebase

      $rootScope.bakiciCalismaDurumuFon = function () {
        $http.get($rootScope.API+'userList2.php?userID='+$rootScope.local_userDetails.id)
        .success(function(data) {
        //Save to local storage
        localStorage.setItem('userDetails' , JSON.stringify(data[0]));
            //get data from local storage to local variable
            $rootScope.saveLocalVariable();
            //get data from local storage to local variable
        //Save to local storage
    });
    }

    $scope.istenCikartmaSebebi = {};
    $rootScope.aileIseAlAlmaCikartFon = function(iseAl,interviewID,caryID){
      var promptPopup = $ionicPopup.prompt({
       title: 'İşten Çıkarma Nedeni',
       template:  '<ion-radio ng-model="istenCikartmaSebebi.choice" ng-value="1">Uygunsuz Davranış</ion-radio>'+
       '<ion-radio ng-model="istenCikartmaSebebi.choice" ng-value="2">Cinsel Taciz</ion-radio>'+
       '<ion-radio ng-model="istenCikartmaSebebi.choice" ng-value="3">Hırsızlık</ion-radio>'+
       '<ion-radio ng-model="istenCikartmaSebebi.choice" ng-value="4">İhtiyaç Dışı</ion-radio>',
       scope: $scope,
       buttons: [{text: 'İptal',
       type: 'button-assertive'},
       {
        text: '<b>Çıkart</b>',
        type: 'button-balanced',
        onTap: function(e) {
                        console.log($scope.istenCikartmaSebebi.choice); // 0              
                        $http.get($rootScope.API+'AileIseAlAlmaCikart.php?interviewID=' + interviewID + '&iseAl=' + iseAl + '&userID=' + caryID + '&radioValue=' + $scope.istenCikartmaSebebi.choice)
                        .success(function(data) {
                            $rootScope.aileIseAlAlmaCikart=data;
                            if($scope.aileIseAlAlmaCikart == true){
                                if(iseAl == 1)
                                    $rootScope.loadData('İşe Alım Tamamlandı','ion-checkmark-round');
                                else
                                    $rootScope.loadData('İşten Çıkartıldı','ion-close-round');
                                $rootScope.kabulEdenlerFon();
                                $rootScope.closePageFon("homepage"); 
                                sessionStorage.clear(); 
                            }
                            else{
                                $rootScope.loadData('İşe Alım Başarısız!','ion-close-round');
                                window.location.href = "#/app/kabuledenler";
                                sessionStorage.clear(); 
                            }
                        });
                    }
                }]
            });
  }

////////
$rootScope.homePageFon = function (){
    Service.GetHomePage($rootScope.local_userDetails.userType,$rootScope.local_userDetails.id).then(function(data){
        $scope.homepagecounts=data;
        console.log("homepagecounts:" , $scope.homepagecounts);
        console.log("homepagecounts.length:", $scope.homepagecounts.gorusmedeOldugum.length);
    });
}

$rootScope.banaOzelTekliflerFon = function (){
    Service.GetBanaOzelTeklifler($rootScope.local_userDetails.id).then(function(data){
        $rootScope.banaozelteklifler = data;
        console.log("banaozelteklifler: ", $rootScope.banaozelteklifler);
    });
}

$rootScope.kabulEttigimTekliflerFon = function () {
    Service.GetKabulEttigimTeklifler($rootScope.local_userDetails.id).then(function(data){
        $rootScope.kabulettigimteklifler=data;
        console.log("kabulettigimteklifler: ", $rootScope.kabulettigimteklifler);
    });
}

$rootScope.teklifGonderdiklerimFon = function(){
    Service.GetTeklifGonderdiklerim($rootScope.local_userDetails.id).then(function(data){
        $rootScope.teklifgonderdiklerim=data;
        console.log("teklif gönderdiklerim: ",$rootScope.teklifgonderdiklerim);
    })
}

$rootScope.kabulEdenlerFon = function(){
    Service.GetKabulEdenler($rootScope.local_userDetails.id).then(function(data){
        $rootScope.kabuledenler=data;
        console.log("kabul edenler: ",$rootScope.kabuledenler);
    })
}


$rootScope.onaylanmisTekliflerimFon = function(){
    Service.GetOnaylanmisTekliflerim($rootScope.local_userDetails.id).then(function(data){
        $rootScope.onaylanmistekliflerim=data;
        console.log("onaylanmistekliflerim: ", $rootScope.onaylanmistekliflerim);
    })
}

$rootScope.onayBekleyenTekliflerimFon = function(){
    Service.GetOnayBekleyenTeklifler($rootScope.local_userDetails.id).then(function(data){
        $rootScope.onaybekleyentekliflerim=data;
        console.log("onaybekleyentekliflerim: ", $rootScope.onaybekleyentekliflerim);
    })
}

$scope.doRefresh = function(talep) {
    $timeout( function() {
        switch (talep){
            case 'homepage':
            $rootScope.homePageFon();
            $scope.$broadcast('scroll.refreshComplete');
            break;
            
            case 'banaozelteklifler':
            $rootScope.banaOzelTekliflerFon();
            $scope.$broadcast('scroll.refreshComplete');
            break;
            case 'kabulettigimteklifler' : 
            $rootScope.kabulEttigimTekliflerFon();
            $scope.$broadcast('scroll.refreshComplete');
            break;

            case 'tumbakicilar':
            $rootScope.loadMore();
            $scope.$broadcast('scroll.refreshComplete');
            break;
            case 'teklifgonderdiklerim':
            $rootScope.teklifGonderdiklerimFon();
            $scope.$broadcast('scroll.refreshComplete');
            break;
            case 'kabuledenler':
            $rootScope.kabulEdenlerFon();
            $scope.$broadcast('scroll.refreshComplete');
            break;

            case 'teklifolustur':
            $rootScope.teklifOlusturFon();
            $scope.$broadcast('scroll.refreshComplete');
            break;
            case 'onaylanmistekliflerim':
            $rootScope.onaylanmisTekliflerimFon();
            $scope.$broadcast('scroll.refreshComplete');
            break;
            case 'onaybekleyentekliflerim':
            $rootScope.onayBekleyenTekliflerimFon();
            $scope.$broadcast('scroll.refreshComplete');
            break;
            case 'hesabim':
            $rootScope.bakiciCalismaDurumuFon();
            $scope.$broadcast('scroll.refreshComplete');
            break;

            default:
            $scope.$broadcast('scroll.refreshComplete');
            break;
        }
    }, 700);
};

//save to local variable 
$rootScope.saveLocalVariable=function(){
    $rootScope.local_userDetails=JSON.parse(localStorage.getItem('userDetails'));
    console.log("user details:" , $rootScope.local_userDetails);
}
//save to local variable 

//Uyarı ekran
$rootScope.loadData = function(text,icon) {
    $scope.loadingIndicator = $ionicLoading.show({
        template: '<div class="bigger"> <p><i class="icon '+icon+' larger"></i></p></div> '+text
                // template:'<ion-spinner icon="ripple" class="spinner-energized"></ion-spinner> <br/> My Label',
            });   
    $timeout(function() {    
      $ionicLoading.hide();
  }, 1500);
};  
//Uyarı ekran

//if user logged in before
if (localStorage.getItem('userDetails'))
{
    $rootScope.saveLocalVariable();
}
//if user logged in before

//sign out and clear local storage
$rootScope.logout=function(){
    localStorage.clear();
    sessionStorage.clear();
    // $rootScope.closeteklifolusturmodal();
    $rootScope.closePageFon("login");

    $http.get($rootScope.API+'login.php?e='+$rootScope.local_userDetails.email
      +'&type='+ 0)
    .success(function(data) {
     $rootScope.loadData('Çıkış başarılı!','ion-checkmark-round');
 })
}
//sign out and clear local storage



// Form data for the login modal
$scope.loginData = {};

var navIcons = document.getElementsByClassName('ion-navicon');
for (var i = 0; i < navIcons.length; i++) {
    navIcons.addEventListener('click', function () {
        this.classList.toggle('active');
    });
}
        /*
        var fab = document.getElementById('fab');
        fab.addEventListener('click', function () {
            //location.href = 'https://twitter.com/satish_vr2011';
            window.open('https://twitter.com/satish_vr2011', '_blank');
        });
        */
        // .fromTemplate() method
        var template = '<ion-popover-view>' +
        '   <ion-header-bar>' +
        '       <h1 class="title">My Popover Title</h1>' +
        '   </ion-header-bar>' +
        '   <ion-content class="padding">' +
        '       My Popover Contents' +
        '   </ion-content>' +
        '</ion-popover-view>';

        $scope.popover = $ionicPopover.fromTemplate(template, {
            scope: $scope
        });
        $scope.closePopover = function () {
            $scope.popover.hide();
        };
        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.popover.remove();
        });

    //ilk çalıştırılacak apiler
    if($rootScope.local_userDetails !== undefined ){
        $rootScope.onaylanmisTekliflerimFon();
        $rootScope.homePageFon();
    }
    //ilk çalıştırılacak apiler
})
