app.controller('LoginCtrl', function($scope, $stateParams,$rootScope,$http,$ionicLoading,$ionicHistory,$state,$ionicPlatform,$ionicPopup,$ionicModal,$timeout) {
//Register
     
    // Create the register modal that we will use later
    $ionicModal.fromTemplateUrl('templates/register.html', {
      scope: $scope,
      animation: 'slide-in-right'
    }).then(function(modal) {
      $scope.modalKayit = modal;
      console.log("Üyelik modalı yaratıldı");
    });

    // Triggered in the register modal to close it
    $scope.closeKayit = function() {
      $scope.modalKayit.hide();
      console.log("Üyelik modalı kapatıldı");
    };

    // Open the register modal
    $scope.kayit = function() {
      $scope.modalKayit.show();
      console.log("Üyelik modalı açıldı"); 
    };


   // Cleanup the modal when we're done with it
    $scope.$on('$destroy', function() {
        $scope.modalKayit.remove();
    });

    // Perform the register action when the user submits the register form
    $scope.datakayit = {};
    $scope.signup = function() {
      console.log('Kayıt yapılıyor');
      console.log($scope.datakayit);
     

      $http.post($rootScope.API+'signup.php?n='+$scope.datakayit.name
        +'&em='+$scope.datakayit.email
        +'&ph='+$scope.datakayit.phone  
        +'&ps='+$scope.datakayit.pass
        +'&ilAdi='+$scope.datakayit.ilAdi
        +'&ilceAdi='+$scope.datakayit.ilceAdi
        +'&y='+ new Date($scope.datakayit.dogumTarihi).toJSON().slice(0,10))
    .success(function(data) { 
      $scope.response=data; 

        if($scope.response.created=="1"){
          $rootScope.loadData('Kayıt başarılı!','ion-checkmark-round');
        $timeout(function(){ $scope.closeKayit();$scope.datakayit = {}; },1500);  

          //no back option
          $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: true
          });

        }else if($scope.response.name=="1"){
          $rootScope.loadData('Kullanıcı adı zaten kayıtlı!','ion-close-round');
        }else if($scope.response.mail=="1"){
          $rootScope.loadData('Mail adresi zaten kayıtlı!','ion-close-round');

        }else{
          $rootScope.loadData('Teknik ekip ile iletişime geçiniz!','ion-alert-circled');
        }
    })
    };
//Register

//LOGIN
    $rootScope.toggledrag = false;
    // Login formundaki dataları tutuyor
    $scope.loginData = {};      
    $scope.doLogin = function() {

    $http.get($rootScope.API+'login.php?e='+$scope.loginData.email
      +'&p='+$scope.loginData.password
      +'&t='+$rootScope.fcmToken
      +'&type='+1)
    .success(function(data) {
      console.log("login success", data);
    $scope.user=data;
    if($scope.user.id>0 && $scope.user.status!=0){
      
    //Save to local storage
        localStorage.setItem('userDetails' , JSON.stringify($scope.user));
        //get data from local storage to local variable
        $rootScope.saveLocalVariable();
        //get data from local storage to local variable
    //Save to local storage

      //clear login form after login
      $scope.loginData = {};
      $rootScope.loadData('Giriş başarılı','ion-checkmark-round');
      $rootScope.toggledrag = true;
        $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
                lastView = $ionicHistory.backView();
                console.log('Last View',lastView);
      $state.go('app.homepage', {}, {location: "replace", reload: true});
    }else if($scope.user.status==0){
      $rootScope.loadData('Hesabınızın onaylanması gerekmektedir.','ion-minus-circled');
    }else{ 
      $rootScope.loadData('Giriş Başarısız','ion-close-round');
    }
    
    })

    };
//LOGIN



})
